#2
name = "Loren"
age = 26
occupation = "Quality Engineer"
movie = "Reborn Rich"
rating = 99.9

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}")

#3
num1 = 10
num2 = 20
num3 = 30

print(num1 * num2)
print(num1 < num3)
print(num3 + num2)